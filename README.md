# [Drone Runne - Docker](https://github.com/drone-runners/drone-runner-docker) apt packages

### Add repo signing key to apt

```bash
sudo curl -sL -o /etc/apt/trusted.gpg.d/morph027-drone-runner-docker.asc https://packaging.gitlab.io/drone-runner-docker/gpg.key
```

### Add repo to apt

```bash
echo "deb [arch=amd64] https://packaging.gitlab.io/drone-runner-docker drone main" | sudo tee /etc/apt/sources.list.d/morph027-drone-runner-docker.list
```
