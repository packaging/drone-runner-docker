getent passwd drone >/dev/null 2>&1 || adduser \
  --system \
  --shell /bin/bash \
  --gecos 'Drone CI' \
  --group \
  --disabled-password \
  --home /home/drone \
  drone
